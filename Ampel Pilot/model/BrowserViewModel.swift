//
//  BrowserViewModel.swift
//  Ampel Pilot
//
//  Created by Вениамин Веселов on 08.02.2022.
//  Copyright © 2022 Patrick Valenta. All rights reserved.
//

import Foundation
import UIKit
import Photos

class BrowserViewModel {
    
    public var assetList = [BrowserCellViewModel]()
    
    func fetchAllVideos(complete: @escaping ()->()  )
    {
        //let albumName = "Мой альбом"
        let fetchOptions = PHFetchOptions()
        // для выбора отдельной папки
        // fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d ", PHAssetMediaType.video.rawValue )
        
        let allVideo = PHAsset.fetchAssets(with: .video, options: fetchOptions)
        
        allVideo.enumerateObjects {[weak self] (asset, index, stop) in
            let cellViewModel = BrowserCellViewModel(asset: asset)
            guard let selfLocal = self else {
                return;
            }
            selfLocal.assetList.append(cellViewModel)
            if stop.pointee.boolValue || (index == allVideo.count - 1) {
                complete()
            }
        }
        
    }
    
    /// Запрашиваю права к файловой системме, а после проверяю их наличие
    /// - Parameter complete: поступ разрешен
    public func checkPermission(_ complete: @escaping () -> (), error: @escaping () -> ()) {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            complete()
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized  {
                    complete()
                    print("success")
                } else {
                    if #available(iOS 14, *) {
                        if  newStatus ==  PHAuthorizationStatus.limited {
                            complete()
                            print("success")
                        }
                    } else {
                        
                    }
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
            error()
        case .denied:
            // same same
            print("User has denied the permission.")
            error()
        case .limited:
            complete()
            print("User for limited photo library access.")
            
        }
    }
    
    /*func getAllVideoAssets() {
        
        var assets: [AnyHashable] = []
        //Fetch all video assets from Photos
        let assetResults = PHAsset.fetchAssets(with: .video, options: nil) as? PHFetchResult
        //get all assets
        for (PHAsset *asset in assetResults){
            NSLog(@"asset type = %zd", asset.mediaType);
            [assets addObject:asset];
        }
        self.allVideoslistArray = [[NSMutableArray alloc] init];
        //create an instance of PHImageManager
        PHImageManager *manager = [PHImageManager defaultManager];
        for(PHAsset *asset in assets){
            //block of code for represent video assets
            [manager requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                if ([asset isKindOfClass:[AVURLAsset class]]) {
                    NSURL *url = (NSURL *)[[(AVURLAsset *)asset URL] fileReferenceURL];
                    UIImage *thumbnail = [self createThunbnailImage:url];
                    [self.allVideoslistArray addObject:@{@"VideoUrl":url,@"ThumbnailImage":thumbnail, @"VideoAsset":asset}];
                }
            }];
        }//end for loop
    }*/
    
    /*      //Call this method.
     [self startMediaBrowserFromViewController: self usingDelegate: self];


   - (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller usingDelegate: (id <UIImagePickerControllerDelegate, UINavigationControllerDelegate>) delegate{

       if (([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO) || (delegate == nil) || (controller == nil))
           return NO;

       UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
       mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;

       mediaUI.mediaTypes = [[[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil] autorelease];
       mediaUI.allowsEditing = YES;
       mediaUI.delegate = self;
       mediaUI.videoMaximumDuration = 60.0;
       //mediaUI.videoQuality = UIImagePickerControllerQualityTypeLow;

       [controller presentModalViewController: mediaUI animated: YES];
       return YES;
   }*/
}
