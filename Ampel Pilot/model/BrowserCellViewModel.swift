//
//  BrowserCellViewModel.swift
//  Ampel Pilot
//
//  Created by Вениамин Веселов on 11.02.2022.
//  Copyright © 2022 Patrick Valenta. All rights reserved.
//


import Photos
import UIKit

class BrowserCellViewModel {
    private var asset: PHAsset?
    
    public var image: UIImage?
    
    public var loaded: (()->())?
    
    private var isLoaded = false
    
    public var url: URL?
    
    init(asset: PHAsset) {
        self.asset = asset
    }
    
    func extractAsset() {
        if !isLoaded {
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                let imageManager = PHCachingImageManager()
                if let weakSelf = self, let assetLocal =  weakSelf.asset {
                    imageManager.requestAVAsset(forVideo: assetLocal, options: nil, resultHandler: {(asset, audioMix, info) in
                        if let assetItem = asset {
                            let avasset = assetItem as! AVURLAsset
                            weakSelf.url = avasset.url
                            
                            if let image = weakSelf.previewImageFromVideo(with: avasset.url) {
                                weakSelf.isLoaded = true
                                weakSelf.image = image
                                weakSelf.asset = nil
                            }
                            weakSelf.loaded?()
                        }
                        
                    })
                }
            }
        } else {
            loaded?()
        }
    }
    
    func previewImageFromVideo(with url: URL) -> UIImage? {
        
        let asset = AVURLAsset(url: url)
        
        let generateImg = AVAssetImageGenerator(asset: asset)
        generateImg.appliesPreferredTrackTransform = true
        generateImg.maximumSize = CGSize(width: 300, height: 300)
        var time = asset.duration
        time.value = CMTimeValue(0)
        var refImg: CGImage? = nil
        do {
            refImg = try generateImg.copyCGImage(at: time, actualTime: nil)
        } catch {
            return nil;
        }
        var frameImage: UIImage? = nil
        if let refImg = refImg {
            frameImage = UIImage(cgImage: refImg)
        }
        return frameImage
    }
    
    
}
