//
//  FileDetectionViewModel.swift
//  Ampel Pilot
//
//  Created by Вениамин Веселов on 12.02.2022.
//  Copyright © 2022 Patrick Valenta. All rights reserved.
//

import UIKit

class FileDetectionViewModel {
    
    private var url: URL
    
    public func getURL()->URL {
        return self.url
    }
    
    init(url: URL) {
        self.url = url
    }
    
    public var feedback: LightPhaseManager.Feedback {
        return LightPhaseManager.Feedback(sound: true, vibrate:  true)
    }
    

}
