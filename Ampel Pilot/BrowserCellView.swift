//
//  BrowserCellView.swift
//  Ampel Pilot
//
//  Created by Вениамин Веселов on 11.02.2022.
//  Copyright © 2022 Patrick Valenta. All rights reserved.
//

import UIKit

class BrowserCellView: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var check: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func convertToGrayScale(image: UIImage) -> UIImage {

        // Create image rectangle with current image width/height
        let imageRect:CGRect = CGRect(x:0, y:0, width:image.size.width, height: image.size.height)

        // Grayscale color space
        let colorSpace = CGColorSpaceCreateDeviceGray()

        let width = image.size.width
        let height = image.size.height

        // Create bitmap content with current image size and grayscale colorspace
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)

        // Draw image into current context, with specified rectangle
        // using previously defined context (with grayscale colorspace)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        context?.draw(image.cgImage!, in: imageRect)
        let imageRef = context!.makeImage()

        // Create a new UIImage object
        let newImage = UIImage(cgImage: imageRef!)

        return newImage
    }
    
    override var isSelected: Bool {
      didSet {
          check.isHidden = !isSelected

          if let image = self.viewModel?.image {
              if isSelected {
                  imageView.image = convertToGrayScale(image: image)
              } else {
                  imageView.image = image
              }
              
          }
          
      }
    }

    
    public var viewModel: BrowserCellViewModel? {
        didSet {
            self.viewModel?.loaded = {[weak self] in
                self?.update()
            }
            self.viewModel?.extractAsset()
        }
    }
    
    private func update() {
        DispatchQueue.main.async { [weak self] in
            if let image = self?.viewModel?.image, let selfLocal = self {
                
                if selfLocal.isSelected {
                    selfLocal.imageView.image = selfLocal.convertToGrayScale(image: image)
                } else {
                    selfLocal.imageView.image = image
                }
            }
        }
    }
}
