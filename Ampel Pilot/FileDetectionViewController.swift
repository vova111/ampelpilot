//
//  FileDetectionViewController.swift
//  Ampel Pilot
//
//  Created by Вениамин Веселов on 12.02.2022.
//  Copyright © 2022 Patrick Valenta. All rights reserved.
//


import Vision
import AVFoundation
import CoreMedia
import CoreMotion
//import VideoToolbox

import UIKit
import MediaPlayer
import CoreML

class FileDetectionViewController: UIViewController {
    
    @IBOutlet weak var videoPreview: UIView!
    
    @IBOutlet weak var resultsView: UIView!
    
    //let semaphore = DispatchSemaphore(value: 2)
    
    let visualFeedbackView = VisualFeedbackView()
    
    var boundingBoxes = [BoundingBox]()
    
    var colors: [UIColor] = []
    
    let ciContext = CIContext()
    
    var lightPhaseManager: LightPhaseManager!
    
    var player:AVPlayer!
    
    var startTimes: [CFTimeInterval] = []

    var viewModel : FileDetectionViewModel!
    
    var analizeInterval: TimeInterval = 0.1
    
    var timerAnalizator: Timer!
    
    var request: VNCoreMLRequest!
    
    let yolo = YOLO()
    
    lazy var backButton: UIBarButtonItem = {
        let bi = UIBarButtonItem(title: "Отмена", style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnPressed))
        return bi
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = backButton
        // Do any additional setup after loading the view.
        
        if let url = viewModel?.getURL() {
            
            player = AVPlayer(url: url)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.videoPreview.bounds
            self.videoPreview.layer.addSublayer(playerLayer)
            player.play()
            //setupViews()
            setUpVision()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("didAppear")
        NotificationCenter.default.addObserver(self, selector: #selector(self.didPlayToEnd), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        setupViewModel()
        self.timerAnalizator = Timer.scheduledTimer(timeInterval: analizeInterval, target: self, selector: #selector(self.sendToAnalize), userInfo: nil, repeats: true)
    }
    
    @objc func didPlayToEnd(){
        self.lightPhaseManager.feedbackManager.stop()
        timerAnalizator?.invalidate()
        timerAnalizator = nil;
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("didDisappear")
        didPlayToEnd()
        player.pause()
        player = nil
        NotificationCenter.default.removeObserver(self)
        super.viewDidDisappear(animated)
    }
    
    func setUpVision() {

        guard let visionModel = try? VNCoreMLModel(for: yolo.model.model) else {
            print("Error: could not create Vision model")
            return
        }
        
        request = VNCoreMLRequest(model: visionModel, completionHandler: visionRequestDidComplete)
        
        // NOTE: If you choose another crop/scale option, then you must also
        // change how the BoundingBox objects get scaled when they are drawn.
        // Currently they assume the full input image is used.
        
        request.imageCropAndScaleOption = VNImageCropAndScaleOption.scaleFill
    }
    
    func setUpBoundingBoxes() {
        boundingBoxes = [BoundingBox]()
        
        for _ in 0..<YOLO.maxBoundingBoxes {
            boundingBoxes.append(BoundingBox())
        }
        
        // Add the bounding box layers to the UI, on top of the video preview.
        for box in self.boundingBoxes {
            box.addToLayer(self.videoPreview.layer)
        }
        
        // Make colors for the bounding boxes. There is one color for each class,
        // 20 classes in total.
        colors.append(.red)
        colors.append(.green)
    }
    
    func setupYolo() {
        self.yolo.confidenceThreshold = 0.3
        self.yolo.iouThreshold = 0.5
    }
    
    func setupViewModel() {
        //viewModel?.initFetch {
            self.lightPhaseManager = LightPhaseManager(confidenceThreshold: 0, maxDetections: YOLO.maxBoundingBoxes, minIOU: 0.3, feedback: self.viewModel.feedback)
            
            self.setUpBoundingBoxes()
            self.setupYolo()
            
            //self.visualFeedbackView.isHidden = self.viewModel.devScreen
            //self.videoPreview.isHidden = false
            //self.resultsView.isHidden = false
        
            //self.frameCapturingStartTime = CACurrentMediaTime()
        //}
    }
    
    func visionRequestDidComplete(request: VNRequest, error: Error?) {
        if let observations = request.results as? [VNCoreMLFeatureValueObservation],
            let features = observations.first?.featureValue.multiArrayValue {
            
            let boundingBoxes = yolo.computeBoundingBoxes(features: features)
            let elapsed = CACurrentMediaTime() - startTimes.remove(at: 0)
            
            lightPhaseManager.add(predictions: boundingBoxes)

            showOnMainThread(boundingBoxes, elapsed, lightPhaseManager.determine())
        }
    }
    
    func showOnMainThread(_ boundingBoxes: [YOLO.Prediction], _ elapsed: CFTimeInterval, _ phase: LightPhaseManager.Phase) {
        DispatchQueue.main.async {[weak self] in
            // For debugging, to make sure the resized CVPixelBuffer is correct.
            //var debugImage: CGImage?
            //VTCreateCGImageFromCVPixelBuffer(resizedPixelBuffer, nil, &debugImage)
            //self.debugImageView.image = UIImage(cgImage: debugImage!)
            
            self?.show(predictions: boundingBoxes)
            
            switch phase {
            case .red:
                
                self?.resultsView.backgroundColor = UIColor.red.withAlphaComponent(0.5)
                //NSLog("===> обнаружен красный сигнал светофора")
                break;
            case .green: self?.resultsView.backgroundColor = UIColor.green.withAlphaComponent(0.5)
                
                //NSLog("===> обнаружен зеленый сигнал светофора")
                break;
            case .none: self?.resultsView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }
            
            //self?.semaphore.signal()
        }
    }
    
    
    func show(predictions: [YOLO.Prediction]) {
        /*if !self.viewModel.devScreen {
            return
        }*/
        
        for i in 0..<boundingBoxes.count {
            if i < predictions.count {
                let prediction = predictions[i]
                
                // The predicted bounding box is in the coordinate space of the input
                // image, which is a square image of 416x416 pixels. We want to show it
                // on the video preview, which is as wide as the screen and has a 4:3
                // aspect ratio. The video preview also may be letterboxed at the top
                // and bottom.
                let width = videoPreview.bounds.width
                let height = width //* (self.viewModel.capturePreset == .vga640x480 ? (4 / 3) : (16 / 9))
                let scaleX = width / CGFloat(YOLO.inputWidth)
                let scaleY = height / CGFloat(YOLO.inputHeight)
                let top = (videoPreview.bounds.height - height) / 2
                
                // Translate and scale the rectangle to our own coordinate system.
                var rect = prediction.rect
                rect.origin.x *= scaleX
                rect.origin.y *= scaleY
                rect.origin.y += top
                rect.size.width *= scaleX
                rect.size.height *= scaleY
                
                // Show the bounding box.
                let label = String(format: "%@ %.1f", labels[prediction.classIndex], prediction.score * 100)
                let color = colors[prediction.classIndex]
                boundingBoxes[i].show(frame: rect, label: label, color: color)
            } else {
                boundingBoxes[i].hide()
            }
        }
    }
    
    func makeVideoScreen()->UIImage? {
        var refImg: CGImage? = nil
        
        if let asset = self.player.currentItem?.asset, let time = self.player.currentItem?.currentTime()   {
            let generateImg = AVAssetImageGenerator(asset: asset)
            generateImg.appliesPreferredTrackTransform = true
            generateImg.requestedTimeToleranceAfter = kCMTimeZero;
            generateImg.requestedTimeToleranceBefore = kCMTimeZero;
            
            do {
                refImg = try generateImg.copyCGImage(at: time, actualTime: nil)
            } catch {
                return nil;
            }
        }
        var frameImage: UIImage? = nil
        if let refImg = refImg {
            frameImage = UIImage(cgImage: refImg)
        }
        
        return frameImage
    }
    
    func predictUsingVision(pixelBuffer: CVPixelBuffer) {
        // Measure how long it takes to predict a single video frame. Note that
        // predict() can be called on the next frame while the previous one is
        // still being processed. Hence the need to queue up the start times.
        startTimes.append(CACurrentMediaTime())
        
        // Vision will automatically resize the input image.
        let handler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer)
        try? handler.perform([request])
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: Action
    @objc func backBtnPressed() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func sendToAnalize() {
        //semaphore.wait()
        if let image = self.makeVideoScreen() {
            if let pixelBuffer: CVPixelBuffer = image.pixelBuffer(width: Int(image.size.width) , height: Int(image.size.height) ) {
                DispatchQueue.global().async {[weak self, weak pixelBuffer] in
                    if let weakPixelBuffer = pixelBuffer {
                        self?.predictUsingVision(pixelBuffer: weakPixelBuffer)
                    }
                }
            }
        }
    }
}
