//
//  BrowserViewController.swift
//  Ampel Pilot
//
//  Created by Вениамин Веселов on 08.02.2022.
//  Copyright © 2022 Patrick Valenta. All rights reserved.
//

import UIKit

class BrowserViewController: UICollectionViewController {
    
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    
    var viewModel: BrowserViewModel?
    
    var selectIndexPath: IndexPath?
    
    lazy var backButton: UIBarButtonItem = {
        let bi = UIBarButtonItem(title: "Отмена", style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnPressed))
        return bi
    }()
    
    lazy var doneButton: UIBarButtonItem = {
        let bi = UIBarButtonItem(title: "Выбрать", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneBtnPressed))
        bi.isEnabled = false
        return bi
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = backButton
        navigationItem.rightBarButtonItem = doneButton
    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        doneButton.isEnabled = false
        loadIndicator.isHidden = false
        viewModel?.checkPermission({ [weak self] in
            self?.viewModel?.fetchAllVideos(complete: {
                self?.loadIndicator.isHidden = true
                self?.collectionView?.reloadData()
            })
        }, error: { [weak self] in
            self?.loadIndicator.isHidden = true
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return viewModel?.assetList.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! BrowserCellView
        cell.viewModel = viewModel?.assetList[indexPath.row]
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        doneButton.isEnabled = true
        selectIndexPath = indexPath
        return true
    }
    

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    // MARK: Action
    @objc func backBtnPressed() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func doneBtnPressed() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "fileDetectionVC") as? FileDetectionViewController {
            if let indexPath = selectIndexPath {
                let cellBrowserCellView = collectionView?.cellForItem(at: indexPath) as? BrowserCellView
                if let cell = cellBrowserCellView, let viewModel = cell.viewModel, let url = viewModel.url {
                    vc.viewModel = FileDetectionViewModel(url: url)
                    let nv = UINavigationController(rootViewController: vc)
                    nv.view.backgroundColor = .white
                    navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}

extension BrowserViewController : UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width
        let itemSize = (screenWidth / 3) - 10

        return CGSize(width: itemSize, height: itemSize)
    }

}
